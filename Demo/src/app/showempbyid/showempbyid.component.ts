import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrls: ['./showempbyid.component.css']
})
export class ShowempbyidComponent{

  employees: any;
  empId: any;
  emilId: any;
  emp:any;

  constructor(private service :EmpService) {

    this.emilId=localStorage.getItem('emilId');

    // this.employees = [
    //   {empId:101, empName:"Harsha", salary:1212.12, gender:"Male",   country:"India",  doj:"12/21/2018", emailId:"harsha@gmail.com", password:"123"},
    //   {empId:102, empName:"Pasha",  salary:2121.21, gender:"Male",   country:"UK",     doj:"11/22/2017", emailId:"pasha@gmail.com",  password:"123"},
    //   {empId:103, empName:"Indira", salary:2323.23, gender:"Female", country:"US",     doj:"10/23/2016", emailId:"indira@gmail.com", password:"123"},
    //   {empId:104, empName:"Venkat", salary:3232.32, gender:"Male",   country:"India",  doj:"09/24/2015", emailId:"venkat@gmail.com", password:"123"},
    //   {empId:105, empName:"Gopi",   salary:3434.34, gender:"Male",   country:"Canada", doj:"08/25/2012", emailId:"gopi@gmail.com",   password:"123"}
    // ];
  }

  getEmployee() {
    this.service.getEmployeById(this.empId).subscribe((employe: any) =>{
     this.emp=employe;
    })
    
  }
}
