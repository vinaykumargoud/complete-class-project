import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { ShowempbyidComponent } from './showempbyid/showempbyid.component';
import { LogoutComponent } from './logout/logout.component';
import { HeaderComponent } from './header/header.component';
import { ExpPipe } from './exp.pipe';
import { GenderPipe } from './gender.pipe';
import { ProductsComponent } from './products/products.component';
// import { NgxCaptchaModule } from 'ngx-captcha';
import { RecaptchaFormsModule, RecaptchaModule } from 'ng-recaptcha';
import { HttpClientModule } from '@angular/common/http';
import { CartComponent } from './cart/cart.component';


@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    Test2Component,
    LoginComponent,
    RegisterComponent,
    ShowemployeesComponent,
    ShowempbyidComponent,
    LogoutComponent,
    HeaderComponent,
    ExpPipe,
    GenderPipe,
    ProductsComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    // NgxCaptchaModule,
    AppRoutingModule,
    ToastrModule.forRoot({
      positionClass:"toast-top-center"
    }),
    BrowserAnimationsModule,
    FormsModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
