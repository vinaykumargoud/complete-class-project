import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  id : number;
  name: String;
  age : number;

  constructor(){
    this.id=101;
    this.name='vinay';
    this.age=23;
  }

  ngOnInit() {

  }

}
