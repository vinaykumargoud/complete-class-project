import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  isUserLogged: boolean;
  loginstatus:Subject<any>;
  cartItems:any
  
  constructor(private http : HttpClient) {
    this.isUserLogged=false;
    this.loginstatus=new Subject();
    this.cartItems=[];
   }

  getCountries() : any{
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  getAllEmployes(){
    return this.http.get('http://localhost:8085/getEmployees')
  }

  getEmployeById(empId:any){
    return this.http.get('getEmployeeById/'+empId)
  }

  register(employe:any){
    return this.http.post('registerEmployee',employe)
  }

  updateEmploye(employe :any){
    return this.http.put('updateEmployee',employe);
  }

  getDepartments(){
    return this.http.get('getDepartments');
  }

  deleteEmploye(empId:any){
    return this.http.delete('deleteEmployeeById/'+empId)
  }

  empLogin(employe:any):Observable<any>{
    return this.http.get('empLogin/'+employe.emailId+"/"+employe.password);
  }

  getLoginStatus(){
    return this.loginstatus.asObservable();
  }
  
   //Successfully Logged In
   setUserLoggedIn() {
    this.isUserLogged = true;
    this.loginstatus.next(true);
  }

   //Logout
  setUserLoggedOut() {
    this.isUserLogged = false;
    this.loginstatus.next(false)
  }

  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }
  addToCart(product: any) {
    this.cartItems.push(product);
  }
  getCartItems(): any {
    return this.cartItems;
  }
}
