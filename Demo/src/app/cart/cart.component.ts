import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent {
  cartItem:any
  constructor(private service : EmpService){
    this.cartItem=service.getCartItems()
  }

  deleteCartItem(product :any){
    const i=this.cartItem.findIndex((element :any) => {return element.id==product.id})
    this.cartItem.splice(i,1)
  }
}
