import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrls: ['./test2.component.css']
})
export class Test2Component implements OnInit {

  person : any

  constructor(private toastr: ToastrService){
    this.person={
      id:102,name:'kumar',age:23,
      hobbies:["music","cricket",'Swimming','Browsing'],
      address:{streetNo: 101, city:'Hyd', state:'Telangana'}
    }
  }
  ngOnInit() {

  }
  submit(){
    alert("Button Clicked")
    alert(this.person)
    alert("id = "+this.person.id +", name = "+this.person.name)
    console.log(this.person)
  }

  sucsses(){
    this.toastr.success("I am working",'finally')
    this.toastr.error("Hoo its an error...",'Error')
    this.toastr.info('you must do this ','Info')
  }

}
