import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit{

  employee: any;
  coutriesList : any;
  departmentList:any;

  password:string = '';
  Repassword:string = '';

  constructor(private toaster:ToastrService,private router : Router,private service : EmpService) {
   
    this.employee = {
      
      "department": {"deptId":""},
      "empId": "",
      "empName": "",
      "salary": "",
      "gender": "",
      "doj":"",
      "country": "",
      "mobile":"",
      "emailId": "",
      "password": ""
      }
  }
  ngOnInit(){
    this.service.getCountries().subscribe((countriesData: any) => {this.coutriesList = countriesData;});

    this.service.getDepartments().subscribe((departmentData: any) => {
      this.departmentList = departmentData;
      console.log(departmentData);
    });
  }

  employeRegister(regForm:any){
    if (this.employee.Repassword !== this.employee.password) {
      this.toaster.error('Passwords does not matched');
    }else{
      console.log(regForm);

    this.employee.department.deptId = regForm.deptId;
    this.employee.empName = regForm.empName;
    this.employee.salary = regForm.salary;
    this.employee.gender = regForm.gender;
    this.employee.doj = regForm.doj;
    this.employee.country = regForm.country;
    this.employee.mobileNumber=regForm.mobile;
    this.employee.emailId = regForm.emailId;
    this.employee.password = regForm.password;

    this.service.register(this.employee).subscribe((data: any) => {
      console.log(data);
    });
    this.toaster.success('Registered Succsesfully')
    this.router.navigate(['login'])
    }
  }

  validateMail(mail:any):boolean{
    return /[@]/.test(mail);
  }

  validateMobile(mobile: string): boolean {
    return mobile.length == 10 && /[6-9]/.test(mobile[0]);
  }

  validatePassword(password: string): boolean {
    return password.length >= 3 && /[A-Z]/.test(password) && /[0-9]/.test(password) && /[!@#$%^&*()_+{}\[\]:;<>,.?~\\\-]/.test(password);
  }

// ReCheckPassword(password:string,Repassword:string):boolean{
//   return password === Repassword;
// }
}
