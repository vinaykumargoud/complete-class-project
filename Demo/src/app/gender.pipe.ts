import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

  transform(value: any,gender:String ): any {

    if(gender == "Male"){
      return "Mr. "+value
    }else{
      return "Miss. "+value
      }
    return value;
  }

}
