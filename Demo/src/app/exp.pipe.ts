import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'exp'
})
export class ExpPipe implements PipeTransform {

  currentYear :any
  joiningYear :any
  Expirence :any

  transform(value: any): any{
    this.currentYear =new Date().getFullYear();
    this.joiningYear =new Date(value).getFullYear();
    this.Expirence =this.currentYear-this.joiningYear;
    return this.Expirence;
  }

}
