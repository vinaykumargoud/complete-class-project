import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

declare var jQuery: any;

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrls: ['./showemployees.component.css']
})
export class ShowemployeesComponent implements OnInit {

  employees: any;
  empObj:any;
  departmentList:any;
  coutriesList:any;

  constructor(private service :EmpService) {

    this.empObj = {
      "department": {"deptId": ""},
      "empId": "",
      "empName": "",
      "salary": "",
      "gender": "",
      "doj": "",
      "country": "",
      "mobile":"",
      "emailId": "",
      "password": ""
    }
  }
  ngOnInit() {
    this.service.getAllEmployes().subscribe(employe =>{
      // console.log(employe)
      this.employees=employe;
    });
    this.service.getCountries().subscribe((countriesData: any) => {this.coutriesList = countriesData;});

    this.service.getDepartments().subscribe((departmentData: any) => {
      this.departmentList = departmentData;
      console.log(departmentData);
    });
  }

  editEmp(emp:any){
    this.empObj=emp;
    jQuery('#empModal').modal('show');
  }
  updateEmploye(){
    this.service.updateEmploye(this.empObj).subscribe(data=>{})
    jQuery('#empModal').modal('hide');
  }

  deleteEmployee(empId:any){
    this.service.deleteEmploye(empId).subscribe((data: any) => {console.log(data);});
  
    const i = this.employees.findIndex((employee: any) => {
      return employee.empId == empId;
    });

    this.employees.splice(i, 1);
  }

  
  }
 
